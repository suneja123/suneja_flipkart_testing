package com.testing;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class FlipkartTesting {
    public static WebDriver driver;
    public static String driverPath = "/home/lenovo/Downloads/chromedriver_linux64/chromedriver";
    public static String kidWear = "kidswear";
    private static final String sSearchBox = "_3704LK";
    public static void initWebDriver(String URL) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        driver.get(URL);
        driver.manage().window().maximize();
    }
    public static void main(String[] args){
        try {
            initWebDriver("https://www.flipkart.com/");
            driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']")).click();
            Thread.sleep(10000);
            //serching item
            driver.findElement(By.className(sSearchBox)).sendKeys(kidWear , Keys.ENTER);
            Thread.sleep(1000);
            //select item
            driver.findElement(By.xpath("//a[@class='_2UzuFa']")).click();
            nextTab();
//            ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
//            driver.switchTo().window(tabs.get(tabs.size() - 1));
            Thread.sleep(1000);
            //select size
            driver.findElement(By.xpath("//a[@class='_1fGeJ5 _2UVyXR _31hAvz']")).click();
            Thread.sleep(1000);
            //add cart
            driver.findElement(By.xpath("//button[@class='_2KpZ6l _2U9uOA _3v1-ww']")).click();
            Thread.sleep(1000);
            //button[@class='_2KpZ6l _2ObVJD _3AWRsL']
            //place order
            driver.findElement(By.xpath("//button[@class='_2KpZ6l _2ObVJD _3AWRsL']")).click();
            Thread.sleep(1000);
            //insert mobile number
            driver.findElement(By.xpath("//input[@class='_2IX_2- _17N0em']")).sendKeys("1234567890" ,Keys.ENTER);
            Thread.sleep(2000);
            //closing window
            driver.close();
            driver.quit();
        }
        catch (Exception e){
            System.out.println(e.getClass().getName());
        }
        
        
    }
   public static void nextTab(){
        String parent=driver.getWindowHandle();
        Set<String> s=driver.getWindowHandles();
        Iterator<String> I1= s.iterator();
        while(I1.hasNext())
        {
            String child_window=I1.next();
            if(!parent.equals(child_window))
            {
                driver.switchTo().window(child_window);
                System.out.println(driver.switchTo().window(child_window).getTitle());

            }
        }

    }
}
